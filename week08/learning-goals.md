## I can explain the problem that NAT solves, and how it solves that problem.
Network Address Translation (NAT) solves the issues of the a limited number of IPv4 addresses and the difficulties that arise when an ISP needs to reallocate blocks of IP addresses to growing subnets. With NAT, a private network with private addresses can be created with only one outward facing IP address. Through the use of a NAT translation table and port numbers, a NAT router can forward packets to the correct host.

<ins>Example NAT translation table:</ins>
|      WAN side      |     LAN side      |
|:------------------:|:-----------------:|
|24.34.112.235, 4001 | 10.0.0.1, 3824 |
|24.34.112.235, 4002 | 10.0.0.2, 4322 |
|24.34.112.235, 4003 | 10.0.0.3, 5478 |

In this case, the NAT router is assigned the IP address 24.34.112.235 from the ISP and it then assigns the 3 hosts on the private network to 10.0.0.1, 10.0.0.2, and 10.0.0.3. Say, host a wants to send a packet out to a web server on port 80, Host A assigns a port number, 3824, and sends the datagram into the LAN. The NAT router generates a new source port number (4001) and replaces the source IP with its WAN side IP (24.34.112.235). Now, when the web server responds, the NAT router indexes the table until it matches the port number 4001 to 3824 and the gets the correct destination IP of 10.0.0.1. It rewrites the datagrams destination address and destination port then forwards the datagram into the home network!

## I can explain important differences between IPv4 and IPv6.
IPv4 is 32 bits which means it is limited to 2^32 IP addresses. With so many new subnets and IP nodes being attached to the internet all available IPv4 addresses will be used up. This is where IPv6 comes in and the most important differences are found in the datagram format.
- *Expanded Addressing Capabilities*: IPv6 increases the IP address size to 128 bits. IPv6 introduces anycast address that allows a datagram to be any one of a group of hosts.
- *A StreamLined 40-byte header*: Fixed length header allows for faster processing of the IP datagram by a router
- *Flow Labeling*: Allows for labeling of packets for which the sender requests special handling. i.e audio/video transmission could be treated as flow. Or a user paying for better server for their traffic

## I can explain how IPv6 datagrams can be sent over networks that only support IPv4.
IPv6 datagrams can be sent over IPv4 networks through a process called **tunneling**. The IPv6 node on the sending side puts the entire IPV6 datagram in the payload field of an IPv4 datagram. The IPv4 datagram is then addressed to the IPv6 node on the receiving side of the tunnel and sent among the IPv4 routers as if it is a normal datagram until it reaches the destination IPv6 node. The receiving IPv6 datagram determines that IPv4 datagram contains an IPv6 datagram by seeing the protocol number in the IPv4 datagram is 41 which indicates that the payload is an IPv6 datagram. It extracts it and then keeps forwarding it as usual.  

Here is a nice diagram from the book!

![Alt text](image-3.png)