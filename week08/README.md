## Week 08 More Data Plane 
### Owen Pitchford
- [Learning Goals](week08/learning-goals.md)
- [Vegetables](week08/questions.md)
- [Deep Dive](week08/lab/deep-dive.md)
- [Feedback](https://gitlab.com/anuj-sirsikar/ic332-portfolio/-/issues/14)
