# Week 12: Link Layer

- [Learning Goals](learning-goals.md)
- [Questions](questions.md)
- [Lab](lab.md)
- [Feedback](https://gitlab.com/usna-ic322/ic322-portfolio/-/issues/22)