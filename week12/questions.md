# Review Questions

## **R4.** Suppose two nodes start to transmit at the same time a packet of length L over a broadcast channel of rate R. Denote the propagation delay between the two nodes as d<sub>prop</sub> Will there be a collision if d<sub>prop</sub> < L/R? Why or why not?
If the propagation delay is less than the transmission delay (L/R), then there will be in a collison because one node will still be transmitting while it starts receiving the packet from the other node. 

## **R5.** In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
<ins>Slotted Aloha:</ins>  
1. Allows a node to tranmist continously at the full rate, R, when that node is the only active node.
2. Successful throughput of a 100 Mbps channel rate will be less than 37 Mbps so not quite R/M
3. Highly decentralized but not fully because slots need to be syncronized in all nodes.
4. Extremely simple protocol

<ins>Token Passing:</ins>  
1. If a node has framed to transmit when it receives the token, it sends up to a max number of frames
2. Highly efficient
3. No master node meaning it is decentralized but failure of one node can crash the entire channel
4. 

## **R6.** In CSMA/CD, after the fifth collision, what is the probability that a node chooses K=4? The result K=4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?
After the 5th collision, K is chosen with equal probability from {0, 1, 2,..., 2<sup>5</sup>-1}. That means the probability if choosing K = 4 is 1/32. Delay is `K*512` so `4*512 = 2048 bits`. On 10 Mbps Ethernet, this is 204.8 microseconds.

# Problems

## **P1.** Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.
~~~
1 1 1 0 | 1  
0 1 1 0 | 0  
1 0 0 1 | 0  
0 1 0 1 | 0  
_ _ _ _ _
0 1 0 0   0  
~~~
## **P3.** Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.
```
 01001001 01101110
+01110100 01100101
------------------
 10111101 11010011
+01110010 01101110
------------------
 00110000 01000010 (overflow, wrap around by adding the 1)
+01100101 01110100
------------------
 10010101 10110110

 01101010 01001001 (one's complement of sum which is the internet checksum)
```

## **P6.**  Consider the 5-bit generator, `G = 10011` and suppose that D has the values below. What is the value of R?
- 1000100101: R = 10001 ![Alt text](image-2.png)
- 0101101010: R = 1 ![Alt text](image.png)
- 0110100011: R = 01 ![Alt text](image-1.png)

## **P11.** Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.

### a. What is the probability that node A succeeds for the first time in slot 4?
4p(A)(1 - p(A))<sup>3</sup>  
p(A) = p(1 - p)<sup>3</sup> (Prob that A transmits, p, and the rest do not, (1-p)<sup>3</sup>)  
4(p(1-p)<sup>3</sup>)(1 - (p(1-p)<sup>3</sup>))
### b. What is the probability that some node (either A, B, C or D) succeeds in slot 5?
4p(1-p)<sup>3</sup>

### c. What is the probability that the first success occurs in slot 4?
(3 * (1 - 4p(1-p)<sup>3</sup>)) * 4p(1-p)<sup>3</sup>  

Essentially, 3 * no node succeeds in a slot * one succeeds
### d. What is the efficiency of this four-node system?
Efficiency is the probability of a node successfully getting a slot  
4p(1-p)<sup>3</sup>  


## **P13.** Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is d<sub>poll</sub>. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?
Length of polling round = N(Q/R + d<sub>poll</sub>)  
The max number of bits transmitted in a round is NQ so the max throughput is NQ/N(Q/(R + d<sub>poll</sub>))  