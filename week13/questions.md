# Chapter 6
# Review Questions

## **R10.** Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?
C's adapter will process these frames from node A in the first case but it will not pass the IP datagrams in these frames up to the network layer. However if A sends frames with the MAC broadcast addresss, C's adapter will process the frames and send it up to the network layer.  

## **R11.** Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
An ARP query has to be sent within a broadcast frame because the querying host does not know the MAC address of the destination host. Using the broadcast MAC address will make sure that the frame reaches the destination host. An ARP response is sent within a frame with a specific destination MAC address becasue the MAC address of the original sender is known since they sent the ARP query.  
# Problems

## **P14.** Consider three LANs interconnected by two routers, as shown in Figure 6.33.


### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
Subnet 1 -> A: 192.168.1.25  B: 192.168.1.5
Subnet 2 -> C: 192.168.2.25  D: 192.168.2.5
Subnet 3 -> E: 192.168.3.25  F: 192.168.3.32


### b. Assign MAC addresses to all of the adapters.
A: A0-00-00-00-00-00
B: B0-00-00-00-00-00
C: C0-00-00-00-00-00
D: D0-00-00-00-00-00
E: E0-00-00-00-00-00
F: F0-00-00-00-00-00

### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.
E realizes B is not in its subnet via DHCP
E creates IP datagram with dest. B
E creates link layer frame containing datagram from step 2 -> dest = R2 MAC addr = R2
R2 receives, sends to R1 (MAC of R1)
R1 removes datagram, determines outgoing interface based on destination IP. Sets dest. MAC to B
R1 creates link layer frame and sends to B
B receives


### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).
source makes IP datagram for dest.
source broadcasts MAC asking for gateway IP
switch broadcasts
gateway responds with MAC
switch forwards to the source
source sends with dest. IP of dest. and dest. MAC of dest.

## **P15.** Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.

### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?
No, because host E and F are in the same subnet.
Source IP/MAC: E IP/E MAC
Destination IP/MAC: F IP/F MAC


### b. Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP and MAC addresses?
No, but host E will send an ARP query to R1.
Source IP/MAC: E IP/E MAC
Destination IP/MAC: B IP/R1 LAN MAC


### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?
S1 will forward the ARP query message on both of its interfaces and S1 will learn where host A resides and add host A to its forwarding table.
Yes, R1 will receive the ARP query message.
No, R1 will not forward the ARP query message.
Host B will not need to send an ARP query message as it receives host A's MAC address from the original ARP query message. S1 will add host B to its forwarding table and drop the ARP response message.

# Chapter 7
# Review Questions
## **R3.** What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?
**<ins>Path Loss:</ins>**  When a given signal disperses which results in decreased signal strength as the distance between the sender and receiver increases.   
**<ins>Multipath Propagation:</ins>** occurs when portion of the EM wave reflect off objects and the ground, taking paths of different lengths between the sender and receiver. 
**<ins>Interference:</ins>** Caused by other sources trasmitting on the same frequency or from EM noise withing the environment (a motor or microwave).   
## **R4.** As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?
1. Increase the transmission power to increse signal-to-noise ratio and lower bit error rate.
2. Reduce the tramsission rate to decrease bit error rate.
# Problems 

## **P6.** In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?
If station was transmitting many frames and started at step 1, any other station trying to transmit would never sense an idle channel and therefore never get to transmit until station 1 finished sending all of its frames. The designers most likely wanted a sense of fairness between stations and therefore made each station go back to step 2 and wait a random backoff time. 
## **P7.** Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.
```
802.11b = 11 Mbps transmission rate. 
An empty control frame is 32 bytes long or 256 bits. So RTS, CTS, and ACK = 256 bits/11 Mbps = 23 usec
DATA = 1500 bytes so 12000 bits + 256 bits of frame so 12256 bits. 12256 bits/11 Mbps = 1114 usec
DIFS + 3SIFS + (3*23 usec) + 1114 usec = 
DIFS + 3SIFS + 1183 usec
``` 