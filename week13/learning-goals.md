## I can describe the role a switch plays in a computer network.
The switch receives incoming link-layer frames and forwards them onto outgoing links while being transparent to the hosts and routers in the subnet. Essentially a host or router addresses a frame to another host or router and sends the frame into the LAN without explicitly addressing the switch to forward the frame. Switches perform the function of *forwarding* and *filtering*. A switch filters frames to decided whether the frame should be dropped or forwarded to some interface. Forwarding is the switch function that determines the interfacese to a which a frame should be directed, and then moces the frame to that interface. It does this through switch table.  

**Example Switch table**
|Adresss|Interface|Time of entry|
|-------|---------|----|
|62-FE-F7-11-89-A3|1|9:32|
|7C-BA-B2-B4-91-10|3|9:36|  

Switches are plug-and-play devices that require no intervention of a network administrator or user. All that has to be done is connect the LAN segments to the swtich and the switch table will configure itself. 

## I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.
Address Resolution Protocol (ARP) solves the problem of translating between network-layer IP addresses and link-layer MAC addresses. To solve this problem an ARP module in the sending host takes any IP address on the same LAN as input and returns the corressponding MAC address, similar to how DNS returns the hostname for an IP address. To do this, it makes use of an ARP table that contains the Host IP address, host MAC address, and TTL. This table is configured automatically like a switch table. 

In a simple LAN scenario, if a host wants to send a datagram to another host in the same subnet, it must get the MAC address of the destination host via the IP address. If the sending host's ARP table already has an entry for the destination host then it just performs a simple lookup. If not, the sending host sends an ARP query packet that is directed ot the MAC broadcast address which is FF-FF-FF-FF-FF-FF. Each host in the subnet then passes the frame up to their ARP module to check and see if its IP address matches the destination IP address in the ARP query packet. Once it finds a match, the destination host sends back an ARP response packet which the sending host will receive and update their ARP table to send the IP datagram.
## I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.
CSMA/CS is a random access protocol for 802.11 wireless LANs. As with Ethernet, CSMA stands for Carrier Sensing multiple access which means that each station senses the channel before transmitting and will not transmit if the channel is sensed to be in use. Unlike Ethernet, however, 802.11 uses collision avoidance techniques instead of collision detection. Additonally, becasue of relatively high bit error rates of wireless channels, 802.11 uses a link-layer acknowledgement/retransmission scheme known as ARQ. 

802.11 does not use collison detection for 2 reasons:
1. The adapter cannot transmit and listen at the same time. It's costly to build hardware to detect a collsion since the strength of the recieved signal is very small compared to the strength of the transmitted signal at the 802.11 adapter.
2. The adapter would not be detect all collsions due to the hidden terminal problem. 

Since collison detection is not used, 802.11 wireless LANs have to transmit the entire frame once it starts. The four steps of CSMA/CA are as follows:
1. If the station initally senses an idle chaneel, it transmits the fram after a short period of time known as DIFS.
2. Otherwise, the station chooses a random backoff value using binary exponential backoff and counts down this value after DIFS when the channel is sensed idle. If the channel is busy, the counter is frozen.
3. When the counter reaches 0, station transmits the entire frame and waits for an ACK.
4. If ACK is recieved, the transmitting station knows the frame has been correctly received. If it has another frame to send it restarts at step 2. If ACK is not received, station reenters teh backoff phase is step 2 with the random value chosen from a larger interval.

**RTS/CTS**: RTS and CTS stand for request to send and clear to send. These are signals sent by the host to request a channel which prevents collisions from occurring. 