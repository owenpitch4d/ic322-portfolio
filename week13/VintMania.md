# Questions
1. Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
Vint Cerf is the co-designer of the TCP/IP protocols and the architecture of the internet. He designed the software code used to transmit data over the internet. His partner, Robert Khan is the other "father" of the internet. 
2. Find 3 surprising Vint Cerf facts.
- His hobbies incolude fine wine, gourmet cooking, and science fiction.
- He is designing an interplanetary internet
- Since 1988 Vint Cerf lobbied for the privatization of the internet.
3. Develop 2 interesting and insightful questions you'd like to ask him.
- Could you share your vision for how an interplanetary internet will might impact, both good and bad, space exploration and its scientific research?
- What was your biggest barrier to the creation of the iternet and how did you overcome it? Additonally, challenges do you expect the internet will face in the upcoming decade.  