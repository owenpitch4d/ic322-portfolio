# Review Questions 

## **R20.** In the TLS record, there is a field for TLS sequence numbers. True or false?
False. The fields are Type, Version, Length, Data, HMAC
## **R21.** What is the purpose of the random nonces in the TLS handshake?
Random nonces in the TLS handshake prevent a connection replay attack because the host server will send different encryption keys despite the same sequence of messages. 
## **R22.** Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.
True. Initilization Vectors are sent in clear and occur during step 4 of the TLS handshake.
## **R23.** Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?
The last step when Bob receives Trudy's HMAC. Since trudy couldn't have decrypted Bobs PMS that was encrypted with Alice's public key because she doesn't have Alice's private key that HMAC's will not match up and Bob will discover it is not actually Alice. 
# Problems
## ![Alt text](image.png)
### ![Alt text](image-1.png)
![Alt text](image-5.png)
### ![Alt text](image-2.png)
![Alt text](image-6.png)
### ![Alt text](image-3.png)
![Alt text](image-7.png)
### ![Alt text](image-4.png)
![Alt text](image-8.png)
## **P14.** The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?
I think a MAC is chosen because it requires less storage, since public key infrastructure is not used. Additonally, all routers are in the same domain so the shared secret key can be easily shared to each router. 

## **P23.** Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.
R2 detection of the duplicate datagram depends on if Trudy updates the sequence number. If Trudy does not update the sequence number then R2 will detect the duplicate when it checks the sequence number in the ESP header. If trudy increments the sequence number, R2 will not detect it. 