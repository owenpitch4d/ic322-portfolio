## I can describe how Link State algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Link State algorithm.
**How it Works**  
- In an LS algorithm, the network topology and all link costs are known. Each node broadcasts link state packets that contains the identites and costs of its attached links to all other nodes in the network. Each node then runs the LS alg and computes the same set of least cost paths as every other node.  

**Advantages**  
- Algorithm has complete information about connectivity and link costs
- The calculation can be performed on site at a logically centralized controller or could be replicated in each router.

**Common Problems and Solutions**  
- The LS algorithm has a worst-case complexity of O(n^2) but if implemented using a heap, it can O(log(n))
- Oscillations can occur but can prevented if not all routers run the LS alg at the same time. Self-synchronization between the routers can occur even if they initially execute the alg with the same period but at different instants of time. To account for this, each router needs to randomize the time it sends out a link advertisement.

**Protocols based on Link State algorithm**  
- OSPF

## I can describe how Distance Vector algorithms work, their advantages, their common problems and how the problems are addressed. I can also give an example of a protocol based on the Distance Vector algorithm.

**How it Works**  
- The DV algorithm is iterative, asynchronous, and distributed. It is *distrubuted* in that each node receives some information from on or more of its DIRECTLY attached neighbors, performs a calculation, and then distributes the results back to its neighbors. Each node maintains the cost to each directly attached neighbor, its distance vector and the distance vectors of each of its neighbors. Each time a node receives a new distance vector from its neighbors, it saves it and then updates its own distance vector based on the Bellman-Ford Equation. 

**Advantages**  
- This process is self-terminating and will just stop when no more information is exchanged.
- It is asynchronous so it does not require all nodes to operation in lockstep with eachother. 

**Common Problems and Solutions**  
- Takes more time to for changes in the network to propograte across the network so the DV algorithnm is not feasible for large, complex networks. [source](https://www.sciencedirect.com/topics/computer-science/distance-vector-protocol#:~:text=The%20main%20drawback%20to%20distance,for%20larger%2C%20more%20complex%20networks.)
- *Count to infinty problem:* When link between B and C is disconnected, B will remove C from its table but before it can send any updates, A could send an update to B will advertise it can get to C at a cost of 2. B can get to A at a cost of 1 and will update a route to C at a cost of 3. A will then recieve updates from B and update the its cost to get to C to 4. This continues going towards infinity

![Alt text](image.png).
 
Split horizon with routing poison and hold-down timers are used to mitigate loops in RIP. [source](https://www.geeksforgeeks.org/route-poisoning-and-count-to-infinity-problem-in-routing/)
**Protocols based on Link State algorithm**  
- RIP
- BGP