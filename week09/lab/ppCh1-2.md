# Owen Pitchford

# Introduction
For this week's lab, I went back and completed chapters 1 and 2 of protocol pioneer.

# Collaboration
I worked with Anuj Sirsikar.

# Process
To install protocol pioneer, I followed the provided instructions on the course website and on the gitlab repository.

# Questions
### Solving Chapter 1
**Winning Strategy**: ``` self.send_message("Message Received", "W") ```
This was the change I made and it worked because the ship was originally sending a mayday message to the north but the Mothership needed all responsive units to send a message saying message received. I changed the text to the *Message Recieved* and the direction to *W* because the mothership is west of the damaged ship.

### Solving Chapter 2
**Winning Strategy**:
```   
self.state["received"][m.interface][0] += 1   
self.state["received"][m.interface][1] += int(m.text)  

if(self.state["received"][m.interface][0] == 3):  
    self.send_message(str(self.state["received"][m.interface][1]), m.interface)  
```

I added this code into the while loop and each time it receives a message, it stores it it in the correct list. Once the message count reaches 3, it sends the total sum. 

### Feedback
I did not experience any bugs besides having to the restart the kernal and open all sources a couple of times. Other than that, I had fun solving the problems and liked the dialogue. This game is a good edition!
