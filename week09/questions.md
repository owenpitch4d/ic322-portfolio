# Review Questions

## **R5.** ​What is the “count to infinity” problem in distance vector routing?
When link between B and C is disconnected, B will remove C from its table but before it can send any updates, A could send an update to B that will advertise it can get to C at a cost of 2. B can get to A at a cost of 1 and will therefore update a route to C at a cost of 2 + 1. A will then recieve updates from B and update the its cost to get to C to 4. This continues going towards infinity

![Alt text](image.png).
 
## **R8.** True or false: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
False, OSPF is a protocol that uses a link state algorithm and nodes know the network topology for all other nodes in the network. When a node sends out a link state advertisement, it goes to every node in the network and not just the nodes directly attached to it. 

## **R9.** What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?
An area in an OSPF AS is comprised of all of routers that broadcast their link state to eachother. Each area runs its own LS algorithm. The concept of area was introduced for scalability. As the numbers of routers become larger and larger the overhead involed in communicating, computing, and storing router information becomes impossibles. Areas make it smaller networks can communicate with eachother and only have one router or area as the backbone for communication outwards.

# Problems

## **P3.** Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.​ ![Alt text](image-1.png)
![Alt text](image-6.png)
## **P4.**  Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following: Compute the shortest path from v, y, and w to all network nodes.
### V
![Alt text](image-7.png)
### Y
![Alt text](image-8.png)
### W
![Alt text](image-9.png)
## **P5.** Consider the network shown below, and assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node z. ![Alt text](image-3.png)
![Alt text](image-10.png)
## **P11.** Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z, w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm. ![Alt text](image-5.png)

### **a.** When the distance vector routing is stabilized, router w, y, and z inform their distances to x to each other. What distance values do they tell each other?
![Alt text](image-11.png)
### **b.** Now suppose that the link cost between x and y increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.
![Alt text](image-12.png)
### **c.** How do you modify c(y, z) such that there is no count-to-infinity problem at all if c(y, x) changes from 4 to 60?
You can get rid of the link between y and z. 