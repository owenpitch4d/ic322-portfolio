## I can explain how TCP and UDP multiplex messages between processes using sockets.
<ins>Multiplexing:</ins> The job of gathering data chunks into segment at the source host, attaching header information through encapsulation, and passing the segment into the network layer.    
<ins>Demultiplexing:</ins> the job of delivering data in the transport layer segement to the correct socket. The trasport layer examines fields in the segment (dst port and src port) to determine the recieveing socket and and then directs the segement to that socket.

### UDP
UDP socket is defined by a two tuple (destination IP, destination Port number)  

Suppose Host A has two processes running that want to send data to two different process running on Host B each with their own UDP socket and associated port number. The transport layer creates a transport layer segment with the application data, dst port, and src port for each chunk of data being sent. Muliplexing occurs when these messages are grouped together and sent as one into the network. When the UDP segments arrives from the network at Host B, Host B demultiplexes each segment to appropriate sokcet by looking at the segments destination port number.
### TCP
TCP socket is defined by a four tuple (source IP, source Port, dst IP, dst port).  

When a TCP segment arrives from the network, the host uses all 4 of these values to demultiplex the segment to the right host. The differs from UDP because two arriving TCP segments with different source IPs or source port numbers will be directed to two different sockets even if they have the same destination IP and destination port number. 

## I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.
**TCP *(Transmission Control Protocol)*:** Provides reliable, connection-oriented service. Through flow control, sequence numbers, acknowledgements, and tiers, TCP provides reliable data transfer. TCP also provides congestion control by striving to give each connection traversing  a congested link an equal share of the link bandwidth. This is done through regulation of the rate at which TCP connections can send data into the network.  
- Maintains connection state in the end systems with receive/send buffers, congestion-control paramters, and sequence and acknowledgement number parameters. 
- Has 20 bytes of header overhead 
- Necessary for applications that must ensure the entire segment arrives in order to the destination. Suited for applications like email, and HTTP.  

**UDP *(User Datagram Protocol)*:** Provides unreliable, connectionless service. The only services provided by UDP is process-to-process data delivery and error checking by including error-detection fields in its segment's headers. No guarantee that the data sent by one process will arrive intact or at all to the destination. UDP has no congestion control, applications can send at whatever rate they please and for as long as it wants.  

<ins>Scenarios</ins>    
- Good for real time applications since they often require minimum sending rate, they do not want to overly delay segment transmission, and can tolerate some data loss.  
- No delay to establish connection. This is primarily why DNS uses UDP because if it used TCP it would be much slower.
- Server can typically support many more active clients when the application runs over UDP instead of TCP because it does not maintain connection state in the end systems 
- UDP only has 8 bytes of header overhead

## I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.
- <ins>**Sequence Numbers:** </ins> The byte stream number of the first byte in the segment. When a server recieves data from a client it will send back the ACK number of the sequence number of the data. For example: if the client sends data with seq=42, the server will send back ACK=43, saying I have received up to byte 42 and now waiting for bytes 43 and beyond. TCP uses a randomized initial sequence number on both sides for security and functionality.

- <ins>**Duplicate ACKs:**</ins> The ack number that Host A puts in its segment is the sequence number of the next byte Host A is expecting from Host B. The ACK number is saying I have recieved bytes of (ACK number) - 1 and now waiting for bytes (ACK Number) and beyond. A dupilcate ACK is an ACK that reacknowledges a segment for which the sender has already received an earlier ack. This will be sent when an out of order segment arrives with a higher than expected sequence number meaning there was a gap. If the sender receives 3 dupicate ACKS from the receiver, it will retransmit the dropped segment. 

- <ins>**Cumulative ACKs:**</ins> In TCP, the recieving host will pause 2-500 ms after receiving a packet. If it recieves another packet in that time, it will sent one ACK back with seq=(bytes received + 1). This also allows TCP to acknowledge every other packet. 

- <ins>**Timers:**</ins> Timers are used to indicate to the sender when to retransmit a packet because the packet might have been lost, an ACK was lost, or both was delayed. In TCP, the timer is started when the packet is sent and if a timeout occurs bc the ACK was lost or delayed it will retransmit the packet.

- <ins>**Pipelining:**</ins> Multiple segments of data are sent without waiting to recieve an ACK from the server after each segment.

- <ins>**Go-Back-N:**</ins> A sliding window protocol where the sender is allowed to trasmit multiple packets without waitng for ack but is constrained to have no more than some max allowable number N, of unacknowledged packets in the pipeline. In GBN the reciever discards out of order packets if the sequence number that arrives is not what is expected. The reciever will then resend all of packets after the last sequence number or N recived. If an ACK is dropped but another ACK is received after the dropped one, it will do nothing because the reciever only acknowledges the most recently recieved packet. Therefore, the other one with the dropped ACK must've been received. 
 In TCP, out of order packets will be stored in a buffer instead. 

- <ins>**Selective Repeat:**</ins> This differed from GBN in that the sender only retransmits the packets that receieved in error. Each packet will have its own timer because only a single packet will be transmitted on timeout. Packets recieved out of order will be buffered until the missing packet is recieved where it will be placed in the correct location and the buffered packets will then be delivered. Whenever a timer timesouts it will resend that packet. SO if an ACK is dropped, the packet will be resent once the timer timeouts becuase the ACK stops the timer. TCP buffers like SR.

- <ins>**Sliding Window:**</ins> Senders are limited to a max allowable number of unacknowledge packets for flow control and congestion control reasons. Once packets are acked, the window slides and more packets can be sent, 
## I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.
I believe I did a thorough job explaining each of these mechanisms above which cover how they are used to solve these issues in reliable data transfer.