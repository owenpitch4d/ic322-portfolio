## Introduction
In this lab, I examined data transfer through  TCP. I saw how a randomized sequence number is assigned to both sides and how the ACKs are based on the sequence number last received. Additonally, I saw that TCP does not ack each packet it receives but rather and only after a set number of bytes. I also calculated RTT and throughput.  
## Collaboration
N/A
## Process
I followed the process and answered the questions outlined in TCP lab v8.1.
## Lab Questions
### Section 1: Capturing a bulk TCP transfer from your computer to a remote server
I uploaded my alice.txt to the gaia.cs.umass.edu server
### Section 2: Looking at the captured trace
The HTTP POST request was split into 128 TCP segments. The first one was 707 bytes and the rest were 1200 besides the last one because there was not enough bytes for the to reach the max size. 
I am a little confused why the first segment was not 1200 and my research did not come up with anthing useful. I did find 
that 1220 bytes is the Maximum Segment Size for IPv6. The extra 20 bytes are TCP headers.  
1. **What is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu?  To answer this question, it’s probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the “details of the selected packet header window” (refer to Figure 2 in the “Getting Started with Wireshark” Lab if you’re uncertain about the Wireshark windows).**
My IP address is the source IP which is 10.17.34.134. The source port number is 55897.

2. **What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?**
The IP address of the gaia.cs.umass.edu server is 128.119.245.12. It is sending and receiving TCP segments through port 80 which makes sense
because they are HTTP POST requests and HTTP communicated on port 80.

3. **What is the *sequence number* of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? (Note: this is the “raw” sequence number carried in the TCP segment itself; it is NOT the packet # in the “No.” column in the Wireshark window.  Remember there is no such thing as a “packet number” in TCP or UDP; as you know, there are sequence numbers in TCP and that’s what we’re after here.  Also note that this is not the relative sequence number with respect to the starting sequence number of this TCP session.). What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver, see section 3.4.4 in the text)?**  
The raw sequence number is 3875409504 and the relative seq number is 0. The Syn flag is set to 1 which identifies it as a SYN segment. Yes, I think they cna use selective acks but I don't know the why behind it. 

4. **What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value?** 
The raw sequence number of the SYNACK segment is 1932044829. Both the SYN and ACK flags are set to 1 which identifies it as SYNACK segment. The ACK value is 3875409505 which is one more than the SYN sequence number. This is because TCP is saying that have recieved teh segment at 3875409504 and are waiting for the next segment. The SYN segment is 0 bytes which is why the ACK value is only one more. 

5. **What is the sequence number of the TCP segment containing the header of the HTTP POST command?  Note that in order to find the POST message header, you’ll need to dig into the packet content field at the bottom of the Wireshark window, looking for a segment with the ASCII text “POST” within its DATA field,.  How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?**  
The sequence number is 3875409505 which checks out since that is the ACK value of the SYNACK. The connection has been established and the client is now sending data starting at the sequence number that server is expecting next. 707 bytes are in the payload which is not all of the alice.txt file. This segment also uses the PUSH flag which means the reciever should push the data to the receiving application immediately instead of buffering it. 

6. **Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection.**  
- *At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent?*  
    2.505543 seconds for the first segement.
- *At what time was the ACK for this first data-containing segment received?*  
    The first ACK was recieved at 2.530226 seconds
- *What is the RTT for this first data-containing segment?*  
The RTT is found by 2.530226-2.505543. This gives an answer of 0.024683 seconds.
- *What is the RTT value the second data-carrying TCP segment and its ACK?*  
There was no ACK for second, third, or fourth packets. Tehe RTT for the 5th data carrying segment is 0.0247940 seconds.

-*Note: Wireshark has a nice feature that allows you to plot the RTT for each of the TCP segments sent.  Select a TCP segment in the “listing of captured packets” window that is being sent from the client to the gaia.cs.umass.edu server.  Then select: Statistics->TCP Stream Graph->Round Trip Time Graph.*

7. **What is the length (header plus payload) of each of the first four data-carrying TCP segments?**  
The first has a total length of 747 (40 header bytes). Segments two through 4 are 1240 bytes long.

8. **What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments?  Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?**
The available buffer space for each of the first 4 segments is 262656 bytes. I found this in the calculated window size. In this case, the amount of buffer space stayed constant so no it does throttle the sender. 

9. **Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?**  
No, there were no retransmitteed segments. I checked if any of the segments had the same sequence number as another which indicated that it was retransmitted. 

10. **How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu?  Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?**  
The first ACK was sent after the first 10 segments were sent to the receiver. Among the first 10 segments only 2 ACKs were received. One for the first segment, and one for the 5th. The ACKs were 6000 bytes of data apart which is the answer to how much data the reciever acks. Towards the end of the txt file they are acknowledged every 12000 bytes.

11. **What is the throughput (bytes transferred per unit time) for the TCP connection?  Explain how you calculated this value.**  
153026 bytes (total data transmitted) / 0.231685 secs (time when last segment sent - time when first segment sent) = 660491 bytes/sec

12. **Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server.  Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.**  
It seems that it is in its slow start phase the entire time based on the linear increae of packets being sent over time. 
[Graph](image.png)

13. **These “fleets” of segments appear to have some periodicity. What can you say about the period?**  
The fleets seem to be separated by a around 0.02 seconds with the exception of the last fleet which is around 0.45 seconds from the last segment of the last fleet. 
14. **Answer each of two questions above for the trace that you have gathered when you transferred a file from your computer to gaia.cs.umass.edu**  
Also is in slow start phase the entire time for the same reason as above. The fleets are separated by about 0.025 to 0.034.
[My own file](image-1.png)