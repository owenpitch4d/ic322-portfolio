## R5. Why is that voice and video traffic is often sent over TCP rather than UDP in today's internet. 
Some organizations block UDP traffic for security reasons which makes TCP a better choice because TCP traffic is not blocked. Another reason could be that streaming services run on web browsers when communicate with HTTP and HTTP uses TCP. 

## R7. Suppose a processs in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with desintation port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?
Yes, they will because they both have the same destination port. Host C will know that each segment originated from a different host because in the header information for each segment contains the source port. Host A and Host B will have different source ports showing they are different.

## R8. Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.
No, the web server has a welcome socket that creates a separate connection socket for each host to communicate with Host C through. Both of these sockets would have port 80 as their destination port but the source IP address and source port would be specific to Host A and Host B. 

## R9. In our rdt protocols, why did we need to introduce sequence numbers?
The sequence number for a segment is the byte-stream number of the first byte in the segment. These indicate to the recieve whether a new packet is arriving or it is a retranmission.

## R10. In our rdt protocols, why did we need to introduce timers?
Timers are used to indicate to the sender when to retransmit a packet because the packet might have been lost, an ACK was lost, or both was delayed. 

## R11. Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain.
Yes, a timer is still necessary because it would be used to count up to the roundtrip delay. Once it reaches that time and no packet has been recieved, it would invoke a retransmission.

## R12. Visit the Go-Back-N interactive animation at the companion Web site.
I cannot find the animation so I am going to give it my best based on the reading

1. **Have the source send five packets, and then pause the animation before any of the five packets reach the destination. Then kill the first packet and resume the animation. Describe what happens.**   
ACK0 will never be sent which means the 4 other packets will be discarded until the packet0 timeout occurs and all of the packets are retransmitted. 

2. **Repeat the experiment, but now let the first packet reach the destination and kill the first acknowledgment. Describe again what happens.**
I believe it depends on the Window size. The textbook uses N = 4 for the size so I will use that too. If the first ACK is killed, the packets 1-4 will still be recieved. However, there will be an issue with packet 5 because the source must wait after packet 4 for the window to slide once ack1 is recieved. If N = 5 there would be no issue and all packets would be recieved. 
3. **Finally, try sending six packets. What happens?**
If N = 5, then the sender would have to wait until the first ACK is recieved to send the 6th packet.

## P3. UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?
<pre>
 01010011  
+<ins>01100110</ins>  
 10111001  
+<ins>01110100</ins>  
 100101101 --> overflow so wrap around  

 00101101  
+<ins>       1</ins>
 00101110
 </pre>  

 UDP takes the 1s complement of the sum because that becomes the checksum. At the reciever when the 16 (or 8) bit bytes are added together with the checksum, the sum will be 11111111 if there are no errors. If therer is a zero then the reciever would detect an error. All 1 bit errors will be detected but some 2 bit errors will not be detected. For example, imagine the last bit of the first and second byte flipped. So the one became a 0 and the 0 became a 1 in the above example. This error would go unnoticed. 
