## R16. Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a messsage to Bob, who accesses his mail from his mail server us IMAP. Discuss how the message gets from Alics's host to Bob's host. Be sure to list the series of application layer protocols that are used to move the message between the two hosts. 

1. Alice instructs user agent to send message
2. The user agent sends the message to the mail server using HTTP or SMTP where it is placed in message queue.
3. SMTP client side, running on Alice's mail server, sees message in queue and opens a usually direct TCP connection to Bob's mail server i.e. no intermediate servers
4. SMTP handshake, then sends the message into the TCP connection
5. At Bob's server, the server side of SMTP receives the messages and the mail server places the msg in Bob's mailbox
6. Bob invokes his user agent which will use IMAP retrieve the message from Bob's mail server. 

## R18. What is the Head of Line (HOL) blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?
HTTP/1.1 uses persistent TCP connecetiosn, allowing a web page to be sent from server to client over a single TCP connection. However, if, say, a large video clip is near the top the webpage, it can block teh smaller objects behind it as it is passing through the TCP connection. HTTP/1.1 attempts to solve it by opening multiple parallel TCP connections but HTTP/2 wants to get rid of/reduce the number of parallel connection. HTTP/2 solution is to break each message into small frames and interleave the request and response messages on the same TCP connection. For example, the large video would be broken into 1000 equal length frames and the smaller objects broken into 2 frames. The server will receive 9 requests (1 for vid, 8 for smaller objs), and return the first frame of vid and then the first frames of each smaller object. Then, it will send the second frame of the vid and the last frames of each object. In only 18 frames all of the smaller objexts have rendered which decreases user-percieved delay and reduces the amount TCP connections/sockets needed to be open at once. 

## R24. CDNs typically adopt one of two difference server placement philosophies. Name and briefly describe them.
1. *Enter Deep:* Deploy server clusters in access ISPs allo over the world in order to get close to end users. While this does decrease the number of links and routers between the end user and the CDN it can be difficult to maintain and manage all of the clusters due to their high distribution.
2. *Bring Home:* Bring the ISPs home by building by building large clusters at a smaller number of sites. Clusters are typically placed in IXPs. This results in lower maintenance and management overhead but at the cost of higher delay and lower throughput to end users.

## R16. Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a messsage to Bob, who accesses his mail from his mail server us IMAP. Discuss how the message gets from Alics's host to Bob's host. Be sure to list the series of application layer protocols that are used to move the message between the two hosts. 

1. Alice instructs user agent to send message
2. The user agent sends the message to the mail server using HTTP or SMTP where it is placed in message queue.
3. SMTP client side, running on Alice's mail server, sees message in queue and opens a usually direct TCP connection to Bob's mail server i.e. no intermediate servers
4. SMTP handshake, then sends the message into the TCP connection
5. At Bob's server, the server side of SMTP receives the messages and the mail server places the msg in Bob's mailbox
6. Bob invokes his user agent which will use IMAP retrieve the message from Bob's mail server. 

## R18. What is the Head of Line (HOL) blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?
HTTP/1.1 uses persistent TCP connecetiosn, allowing a web page to be sent from server to client over a single TCP connection. However, if, say, a large video clip is near the top the webpage, it can block teh smaller objects behind it as it is passing through the TCP connection. HTTP/1.1 attempts to solve it by opening multiple parallel TCP connections but HTTP/2 wants to get rid of/reduce the number of parallel connection. HTTP/2 solution is to break each message into small frames and interleave the request and response messages on the same TCP connection. For example, the large video would be broken into 1000 equal length frames and the smaller objects broken into 2 frames. The server will receive 9 requests (1 for vid, 8 for smaller objs), and return the first frame of vid and then the first frames of each smaller object. Then, it will send the second frame of the vid and the last frames of each object. In only 18 frames all of the smaller objexts have rendered which decreases user-percieved delay and reduces the amount TCP connections/sockets needed to be open at once. 

## R24. CDNs typically adopt one of two difference server placement philosophies. Name and briefly describe them.
1. *Enter Deep:* Deploy server clusters in access ISPs allo over the world in order to get close to end users. While this does decrease the number of links and routers between the end user and the CDN it can be difficult to maintain and manage all of the clusters due to their high distribution.
2. *Bring Home:* Bring the ISPs home by building by building large clusters at a smaller number of sites. Clusters are typically placed in IXPs. This results in lower maintenance and management overhead but at the cost of higher delay and lower throughput to end users.

## P16. How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.
SMTP marks the end of a message by sending a line consisting of only a period. HTTP uses a content-length header field which indicates the number of bytes in the object being sent. No, HTTP cannot use the SMTP method becuse SMTP has to be 7 bit ASCII but HTTP could be binary or another form. 
## ​P18. a. ​What is a whois database? ​b. Use ​various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.
A database that stores information on domain names and owner information. 
1. https://lookup.icann.org/en/lookup and looked up FACEBOOK.com and got NS.FACEBOOK.COM
2. https://www.whois.com/whois/ and looked up amazon.com and got ns1.amzndns.co.uk for a name server

## P20. Supp​ose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.
Over an extended period of time, consistently look at the DNS cache and store the web servers that are seen in the cache at that time with a counter associated with each server. Each time the DNS cache is looked at, update the counter for web servers in the cache. Whichever web server has the highest count at the end of the time period is the most popular because it will consistenly appear in the cache due to frquent DNS requests from users.

## ​P21. Suppose that your​ department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.
Yes, you can do this by using the dig command. Dig returns the query time when you look up a web site and if the query time is extremely fast this can indicate that it is in the local DNS cache which means someone just accessed it.   