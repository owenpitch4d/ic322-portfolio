## Introduction
For this week's lab, I explored DNS by seeing DNS queries and responses live with wirshark along with the questions and answers that are included. I also got more familiar with the nslookup command which I am sure will greatly benefit me in the future. Using the -type field is something I had never done before.
## Collaboration
N/A
## Process
This is the lab pdf I follwed for every step.
[DNSv8.1](https://docs.google.com/document/d/1w45WT7Ma571zU8rurF4iM8EMuvKjrCjUMvtl8dWrFR0/edit)

Additionally, I have include the wireshark packet captures for each section of questions.
[5-11](https://gitlab.com/owenpitch4d/ic322-portfolio/-/blob/main/week03/lab/DNS_lab_5-11.pcapng)
[12-15](https://gitlab.com/owenpitch4d/ic322-portfolio/-/blob/main/week03/lab/DNS_lab_12-15.pcapng)
[16-18](https://gitlab.com/owenpitch4d/ic322-portfolio/-/blob/main/week03/lab/DNS_lab_16-18.pcapng) 

*Note:* For section 1 of the lab questions I was only the school wifi and not ethernet like I was for the rest of the lab which is why there is variance in the IP addresses. 
## Lab Questions
### Section 1. nslookup
1. Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in.  What is the IP address of www.iitb.ac.in

The IP address is 103.21.124.10.

2. What is the IP address of the DNS server that provided the answer to your nslookup command in question 1 above?

The IP address of the DNS server 127.0.0.53 which I believe is a local DNS server at the academy.

3. Did the answer to your nslookup command in question 1 above come from an authoritative or non-authoritative server?

Non-authoritative

4. Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain.  What is that name?  (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?

**Command:** *nslookup -type=NS www.iitb.ac.in*  
**Output:**  
Server:         127.0.0.53  
Address:        127.0.0.53#53  

Non-authoritative answer:  
*** Can't find www.iitb.ac.in: No answer  

Authoritative answers can be found from:  
iitb.ac.in  
        origin = **dns1.iitb.ac.in**  
        mail addr = postmaster.iitb.ac.in  
        serial = 2013071001  
        refresh = 16384  
        retry = 2048  
        expire = 1048576  
        minimum = 3960  

The name of the authoritative server is dns1.ittb.ac.in. To find the ip address I just ran *nslookup dns1.iitb.ac.in*. It returned the IP address of 103.21.125.129.

### Section 2. The DNS cache on your computer
To clear my DNS cache on my laptop I ran ipconfig /flushdns on windows powershell.

The terminal responded that the DNS resolver cache was successfully flushed.
### Section 3. Tracing DNS with Wireshark
5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message?  Is this query message sent over UDP or TCP?

 The packet number is 121. It was sent over UDP, source port 56417 with destination port 53.

6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message?  Is this response message received via UDP or TCP?

The packet number is 157. It was recieved over UDP with source port and destination port flipped from the query message which makes sense. 

7. What is the destination port for the DNS query message? What is the source port of the DNS response message?

DNS Query Dst Port: 53  
DNS Response Src port: 53  
It checks out that this is the same port. Port 53 is used for both TCP and UDP communication. DNS is a prime example of port 53 use. 

8. To what IP address is the DNS query message sent?

10.1.74.10 

9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
It contains one question and zero answers. 
10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?
One questions and one answer.
11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu.  What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/?  What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address?    What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.  

The intitial HTTP GET request is packet number 209 but the first response was on No.220 with 301 move permanently error. I believe the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu is number 121. The recieved response is packet number 157. The GET request for the image packet number is 343.  
Two other DNS requests were made to bootstrapcdn and jquery. The cdn could have the image stored but I am not sure of this because the responses only come after the GET request for the image. I cannot find a specific DNS request to the DNS cache which is where gaia.cs.umass.edu would be stored. I looked up if a DNS request to a DNS cache would show up on wireshark and I could not find anything so maybe I can't see the request because there was not one due to the web server already being in the cache. 

12. What is the destination port for the DNS query message? What is the source port of the DNS response message?

Dst port: 53
Src port: 53

13. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
10.1.74.10. Yes, it is. 
14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?
Type A and it does not contain any answers.
15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?
1 question and 1 answer.
16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
10.1.74.10 and yes it is.
17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?
1 question and no it does not. 
18. Examine the DNS response message.  How many answers does the response have?  What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?
3 answers. THe information that is returned are three non-authoritative answers meaning they came from a cache. They are three name servers for umass.edu:  
ns2.umass.edu  
ns1.umass.edu  
ns3.umass.edu  

Mine are zero addition resource records but I would assume authoritative answers are included in this field. 



