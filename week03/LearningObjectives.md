## I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.
DNS has to use a large number of servers that are organized in a heirarchial fashion and distributed all around the world.  
**3 Classes of DNS Servers**  
1. *Root DNS Servers* - provide the IP addresses of the TLD servers. More than 1000 root servers that are copies of 13 different root servers. 
2. *Top-level domain (TLD) Servers* - For each TLD like com, net, edu, gov there is TLD server or server cluster. TLD servers provide the IP address of the authoritative DNS servers
3. *Authoritative DNS Servers* - Organizations with publicly accessible hosts (Web or mail servers)  on the Internet must provide publicly accessible DNS records that map the names of those hosts to IP addresses. The organization's own authoritative DNS server holds these records or they pay another to hold it for them. 
4. *Local Server* - Not really part of the heirarchy but still important. Each ISP has a local DNS server to provide IP addresses to hosts that connect to the ISP.  

Once a user enters a request from a web server the user's machine running the client side DNS application will obtain the hostname of the web server from the browser like www.amazon.com. It will first check the Local DNS cache for the hostname and corresponding IP. If it's not there, the DNS client will contact a Root DNS server which will direct the client to the .com TLD DNS server. The TLD DNS server will return the IP for the authoritative DNS server for amazon.com. Finally, the client contacts the authoritative DNS server which returns the IP address for the hostname of www.amazon.com.

## I can explain the role of each DNS record type.
A Resource Record is a four-tuple with the following fields: (Name, Value, Type, TTL)  
Name and value depend on type. TTL is the time to live of the RR. Essentially, it determines when a resource should be removed from a cache.*
**4 Types of DNS Record**
1. <ins>Type A:</ins> Name=hostname and Value is the IP address of the hostname. If a DNS Server is authoritative for a hostname, the DNS server contain a Type A record for the host name.
2. <ins>Type NS:</ins> Name=Domain like example.com and the Value is the hostname of an authoritative DNS server that knows how to obtain the IP addresses of for hosts in the domain.
3. <ins>Type CNAME:</ins> Value is a canonical hostname (CNAME) for the alias hostname Name. Provides querying hosts wiht the canonical name for a hostname. 
4. <ins>Type MX: </ins> Value is a canonical name of a mail server that has an alian hostname Name. Allows for hostnames of mail servers to have simple aliases. Also lets companies have the same aliased name for a web server and mail server. The DNS client would just query the MX record for the cname of the mail server and the CNAME record for the web server. 

## I can explain the role of the SMTP, IMAP, and POP protocols in the email system.
*Note for self:* User agents are for e-mail include MS Outlook, Apple mail, Web-based Gmail, Gmail app, etc.

### Simple Mail Transfer Protocol (SMTP): 
The principal application layer protocol for Internet electronic mail. It uses TCP and acts as both a client, if the mail server is sending mail to other servers, or a server, when the mail server recieves mail from other servers.  

**Simple Example**
1. Alice instructs user agent to send message
2. The user agent sends the message to the mail server where it is placed in message queue.
3. SMTP client side, running on Alice's mail server, sees message in queue and opens a usually direct TCP connection to Bob's mail server i.e. no intermediate servers
4. SMTP handshake, then sends the message into the TCP connection
5. At Bob's server, the server side of SMTP receives the messages and the mail server places the msg in Bob's mailbox
6. Bob invokes his user agent to read the message whenever he wants

**Step 4,5 Deep Dive**  
The client SMTP first establishes a connection to port 25 at the server SMTP. Next, the server and client perform handshaking with the HELO command where the client indicates the e-mail address of the sender (MAIL FROM) and the recipient (RCPT TO). After the introdction, SMTP sends the message (DATA) and relies on TCP to get the message to server with no errors. When the client is done sending messages it instructs TCP to close the connection with the QUIT command.  

### Internet Mail Access Protocol (IMAP):
One of the ways, along with HTTP, that allows the receipient to retrieve his email from a mail server through his user agent. IMAP is typically used with mail clients such as MS Outlook. 

### Post Office Protocol (POP)
Widely used email protocol to retrieve email from a server. POP3 is the most commonly used version. POP3 directly downloads messages to your device while IMAP retains the mesasge on the server. So, POP3 frees up storage space on servers but IMAP frees up space on local hard drives. This section's info was all found on this [website](https://www.indeed.com/career-advice/career-development/pop-protocol-vs-imap-vs-smtp#:~:text=POP3%20and%20IMAP%20are%20protocols,space%20on%20local%20hard%20drives.) because I could not find it in the book. 

## I know what each of the following tools are used for: nslookup, dig, whois.
1. nslookup: used to query any level of DNS server. According the man pages, nslookup can be interactive and non-interactive. Interactive mode allows the user to query name servers for information about various hosts and domains or to print a list of hosts in a domain. Non-interactive mode is used to print just the name and requested information for a host or domain.
2. dig: stands for domain information grouper. Performs DNS lookups and displays the answers that are returned fromm the DNS servers. According to its man pages, DNS admins can use dig to troubleshoot DNS problems bc of its flexibility, ease of use, and clarity of output.
3. whois: used to retrieve info about domain names and IPs from the WHOIS database. This information can include the domain registrar, registration date, expiration data, and the domain's name servers. It also provides contact info for the administrative contact and technical contact. 