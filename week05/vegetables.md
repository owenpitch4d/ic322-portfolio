### R17. Suppose two TCP connections are present over some bottleneck link of rate R bps. Both connections have a huge file to send (in the same direction over the bottleneck link). The transmissions of the files start at the same time. What transmission rate would TCP like to give to each of the connections?
It would like to give R/K where K is the number of TCP connections so in this example, R/2. This means the congestion control mechanism is fair since each connection gets an equal share of the bandwith.

### R18. True or fa​lse? Consider congestion control in TCP. When the timer expires at the sender, the value of ssthresh is set to one half of its previous value.
False, when a timeout occurs the value of ssthresh is updated to half the value of cwnd (congestion window) when the loss event occurred.

### P27.  Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.

#### a. In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?
Sequence number: 207 (127+80)  
Source port number: 302  
Dst port number: 80  
#### b. If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number, the source port number, and the destination port number?
Ack = 207  
Src port number: 80
Dst port number: 302
#### c. If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?
The ACK will be 127 because Host B is expecting up to 126 but it recieves 207 instead. It is still waiting for bytes 127 and onward. 
#### d. Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.
![Image](WEEK5_P27.pdf)

### P33. In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the sampleRTT for retransmitted segments?  
If a segment is sent but the timer expires before an ACK is recieved the segment would retransmit and start a new sampleRTT time. However, if the ACK for the first segment is received, that will stop the RTT calculation and be faster than it actually is. 

### P36. In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?
Probably because when large amounts of packets are sent at once, they could be recieved out of order but thanks to buffering it will be sorted out later. It would be a waste to retransmit after just one duplicate ack because it could be arriving shortly just a little out of order. However, after 3 duplicate acks, it was most likely lost and should be retransmitted.  

### P40.Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.  Figure 3.61
![image](https://cite-media.pearson.com/legacy_paths/a5640ce1-97db-455a-9544-b6a33572624f/Fig03-061.png) 
#### a. Identify the intervals of time when TCP slow start is operating.
TCP slow start is operating during (1,6) and (23,26). You can tell because the amount of segments double each transmission. During TCP slow start, the cwnd increases by 1 MSS for each acknowledged segment.
#### b. Identify the intervals of time when TCP congestion avoidance is operating. 
TCP congestion avoidance is operation over (6,16) and (7,22). The ssthresh must be 32 for the first time.
#### c. After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout? 
By a triple duplicate ACK because if a timeout occurred, the window size would have went to 1.
#### d. After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout? 
A timeout and we can see this because the window size drops to 1. 
#### e. What is the initial value of ssthresh at the first transmission round? 
ssthresh is around 32 because that it when slow start ends and congestion avoidance takes over.
#### f. What is the value of ssthresh at the 18th transmission round? 
The value is 21 because the triple duplicate ack occurred when the window size was 42. After a triple duplicate ack, ssthresh is set to half the cwnd size.
#### g. What is the value of ssthresh at the 24th transmission round? 
It is 14 because the cwnd size is at 29 when the timeout occured. ssthresh is set to half the window size which would be 14 (lower end of 14.5).
#### h. During what transmission round is the 70th segment sent? 
Tranmission 7. It doubles up to tranmission 6 which is up to the 63 segment. The 70th segment would be in the next transmission.
#### i. Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of ssthresh ? 
ssthresh = cwnd/2 => 8/2 = 4  
cwnd = ssthresh + 3MSS = 7
#### j. Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple duplicate ACKs are received at the 16th round. What are the ssthresh and the congestion window size at the 19th round? 
TCP tahoe cuts the cwnd to 1 MSS and enters the slow start phase after either a timeout or a triple duplicate ack.
ssthresh = 21 because it is set to cwnd/2 and the cwnd was at 42 when the timeout occured.  
cwnd = 4 because it enters the slow start phase again at cwnd size goes 1(17th), 2(18th), 4(19th).
#### k. Again suppose TCP Tahoe is used, and there is a timeout event at 22nd round. How many packets have been sent out from 17th round till 22nd round, inclusive?
1(17th) + 2(18th) + 4(19th) + 8(20th) + 16(21st) + 21(22nd) = 52 total packets.  
In the 22nd round 21 packets are sent because congestion avoidance takes over since ssthresh is crossed. 
