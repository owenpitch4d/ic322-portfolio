## I can explain the HTTP message format, including the common fields.
**HTTP REQUEST Example**\
GET /kurose_ross/interactive/index.php HTTP/1.1 <-- Request Line\
Host: gaia.cs.umass.edu <-- Header\
Connection: close       <-- Header
User-agent: Mozilla/5.0 <-- Header\
Accept-language: fr     <--Header\
*Carriage return*\
*Line feed*

The request line has three fields:
1. Method field - GET, POST, HEAD, PUT, DELETE
2. URL field
3. HTTP version field - HTTP or HTTPS

The host line specifies the host on which the object resides.\
The Connection: close is essentially there so the server closes the connection once the requested object is sent\
The Accept-language specifies the desired language the user wants if there is an option available.


**HTTP RESPONSE Message Example**   
HTTP/1.1 200 OK                     <- Status Line (Protocol Version Field, Status Code, Status Msg)\
Connection: Close                   <- Header\
Date: Sat, 28 Aug 2023 12:00:00 GMT <- Header\
Server: Apache/2.4.38 (Unix)         <- Header\
Content-Type: text/html; charset=UTF-8  <- Header\
Content-Length: 1234                <- Header\
                                    (Blank Line)\
(data data data data data...)      <- Entity Body

The header lines are pretty self-explanatory but there are hundreds more. The entity body is the actual requested object.

Link to [ChatGPT](https://chat.openai.com/share/9eadf157-8c91-45b8-abb9-a6f6b165d3e1) for Respose and Request formatting examples
   
## I can explain the difference between HTTP GET and POST requests and why you would use each.
The method field of an HTTP request message is on the request line. These methods include GET, POST,
HEAD, PUT, and DELETE.\
**GET:** The GET method is one of the most common methods used by an HTTP request messasge. It is used when a browser requests an object, with the requested object indentified in the URL field.

**POST:** A major difference between a POST and GET request is that the POST makes use of the entity body field while GET does not. An example of this is when a user provides key search words to a search
engine. While a web page is still being requested from a server the entity body will contain what the user entered into the form fields. A better example is when a user is inputting their username and password into their online bank account. Since the info would be displayed in the entity body of GET request, a POST request is used instead.

*NOTE*: GET requests can also include user inputted data but the URL will just look like\
                    www.somesite.com/animalsearch?monkeys&bananas
## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.
A conditional GET request is used to allow a web cache to verify that its objects are up to date. It solves the issue of a stale object in a cache (an object stored in a web cache that has been modified since since it was cached by the client).

**Requirements of a Conditional GET**
1. The request message uses the GET method
2. The request message includes an If-Modified-Since: header line.

**How a conditional GET operates**\
When a browser requests an object that is already stored in the cache the cache performs an up to date check using a Conditional GET

Host: www.somewebsite.com \
If-modified-since: Wed, 30 Aug 2023 09:23:24 <-- This is from the last-modified field in a response

The conditonal GET is saying to only send the object if it has been modified. If it hasn't the response from the web server will have an empty entity body and the code *304 Not Modified*.


## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.
An HTTP response code indicates the result of the request. They are used to determine what happened with the request. The textbook lists 5 common status codes
1. **200 OK:** Request succeeded and the information is returned in the response
2. **301 Moved Permanently:** Requested object has been permanently moved; the new URL is specified in Location: header of the response message. The client software will automatically retrieve the new URL
3. **400 Bad Request:** This is generic error code indication that the request could not be understood by the server
4. **404 Not Found:** The requested document does not exist on this server
5. **505 HTTP Version Not Supported:** The requested HTTP protocol version is not supported by the server
## I can explain what cookies are used for and how they are implemented.
Cookies are used to allow a website to identify and keep track of users. Most major commercial websites use cookies to possibly restrict user access or to serve specific content as a function of the user identity. Cookies allow websites such as Amazon to suggest certain products and keep track of specific information about the user to improve their shopping experience.  

**4 Components of Cookie Technology**
1. A cookie header line in the HTTP response message
2. A cookie header line in the HTTP request message
3. A cookie file kept on the user's end system and managed by the user's browser
4. A back-end database at the Web site

**Implementation**\
When a client sends a request to a new Web Server, the web server creates a unique id for the user and stores it in their back-end database. Additonally, it will include a *Set-cookie: ####* header line in the HTTP response msg which the browser appends to its cookie file. Now, whenever the user vists the same site the cookie id number will be included in the request msg allowing the server to know which user it is and what specific actions to apply. Clearly this could lead to some privacy issues if the web site sells user information to a 3rd party.


## I can explain the significance HTTP pipelining.
HTTP pipelining allows for multiple requests for objects to be made back-to-back without waiting for a response in between these requests. This then allows the server to send the objects back-to-back.

## I can explain the significance of Content Distribution Networks (CDNs), and how they work.
A CDN manages servers in multiple geographically distributed locations and stores copies of videos, documents, images, and audio in its servers. Specifically for streaming, this prevents internet video companies from having to store all of their videos in one data center and stream the content to all their clients which would be costly, slow, and if it the data center failed no more videos could be streamed. CDNs will therefore direct requests to the a location that will provide the user with the best experience.

**CDN Server Placement Philosophy**
1. *Enter Deep:* Deploy server clusters in access ISPs allo over the world in order to get close to end users. While this does decrease the number of links and routers between the end user and the CDN it can be difficult to maintain and manage all of the clusters due to their high distribution.
2. *Bring Home:* Bring the ISPs home by building b
y building large clusters at a smaller number of sites. Clusters are typically placed in IXPs. This results in lower maintenance and management overhead but at the cost of higher delay and lower throughput to end users. 

The CDN replicates content across its clusters but not everything. Usually, when a client requests a video from a cluster that doesn't have it, it will pull the video from a central repo or from another cluster. It removes videos that are not frequently used.

To operate, the CDN intercepts a request made by a user's browser to determine a CDN server cluster for the client and then redirects the request to a server in that cluster. To do this, CDNs make use of DNS 

<figure>
      <img src=CDN.jpg alt="DNS" width="200">
      <figcaption>DNS redirects a user's request to a CDN server. (Texbook)
</figure>
