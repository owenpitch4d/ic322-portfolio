## Introduction
The main goal of this lab was to investigate the contents of HTTP GET requests and responses to see how a client communicates with a server and how the server responds with the information for the client. During this lab, I also learned more about and saw a Condinitional GET request in action. The other interesting thing was how multiple TCP segments are needed to carry long messages which makes sense because there is maxium TCP segment size to efficiently send the largest packet size over the network.

## Collaboration
N/A

## Process
**Link to Lab:** http://www-net.cs.umass.edu/wireshark-labs/Wireshark_HTTP_v8.0.pdf  
I followed the steps that the labs laid out except for section 2. Instead of using my own wireshark packet capture, I used their example becuase mine did not show a If-modified-since field despite me following all of the directions. I apologize for using v8.0, I could not get the v8.1 to download 

## Lab Questions
### Section 1: The Basic HTTP GET/response interaction
1. **Is your browser running HTTP version 1.0 or 1.1? What version of HTTP is the server running?**

    Both my browser and the server are running version 1.1.
2. **What languages (if any) does your browser indicate that it can accept to the server?**

    The browser indicates the Accept-language is en-US,en:q=0.9. This means that it accepts US english and English in general. I looked up the 
    meaning of q=0.9 and it is essentially used to indicate the preference of the language on a scale of 0 to 1.
3. **What is the IP address of your computer? Of the gaia.cs.umass.edu server?**
 
    My IP: 10.17.334.134
    Server: 128.119.245.12
4. **What is the status code returned from the server to your browser?**
    
    200 OK
5. **When was the HTML file that you are retrieving last modified at the server?**

    Mon, 04 Sep 2023 05:59:02 GMT
6. **How many bytes of content are being returned to your browser?**

    128 bytes
7. **By inspecting the raw data in the packet content window, do you see any headers within the data that are not displayed in the packet-listing window? If so, name one.**

    Yes, the Date header. 
### Section 2: The HTTP CONDITIONAL GET/response interaction
8. **Inspect the contents of the first HTTP GET request from your browser to the server. Do you see an “IF-MODIFIED-SINCE” line in the HTTP GET?**

    No, there is not one. Since I cleared the cache, the object has no chance of being in it.
9. **Inspect the contents of the server response. Did the server explicitly return the contents of the file? How can you tell?**

    Yes, I can tell because the server responded with 200 OK message as well as te including the text content in the entity body.

10. **Now inspect the contents of the second HTTP GET request from your browser to the server. Do you see an “IF-MODIFIED-SINCE:” line in the HTTP GET? If so, what information follows the “IF-MODIFIED-SINCE:” header?**

    Yes there is an If-modified-since line. The date Tue, 23 Sep, 2003 05:35:00 GMT follows it

11. **What is the HTTP status code and phrase returned from the server in response to this second HTTP GET? Did the server explicitly return the contents of the file? Explain.**

    304 Not Modified is the status code. The server did not return the contents of the file and I can tell because it is not included in the entity body.
### Section 3: Retrieving Long Documents
12. **How many HTTP GET request messages did your browser send? Which packet number in the trace contains the GET message for the Bill or Rights?**
    
    7 GET requests. Packet number 876
13. **Which packet number in the trace contains the status code and phrase associated with the response to the HTTP GET request?**

    Packet number 882.
14. **What is the status code and phrase in the response?**

    200 OK
15. **How many data-containing TCP segments were needed to carry the single HTTP response and the text of the Bill of Rights?**

    4 TCP Segments
### Section 4: HTML Documents with Embeddeed Objects
16. **How many HTTP GET request messages did your browser send? To which Internet addresses were these GET requests sent?**

    3 GET requests. They were sent to\
    128.119.245.12: http://gaia.cs.umass.edu - the initial page address/the pearson.png\
    178.79.137.164: kurose.cslash.net - 8E cover small.jpg\ 
    23.15.9.18 which i think is just an online certificate protocol
17. **Can you tell whether your browser downloaded the two images serially, or whether they were downloaded from the two web sites in parallel? Explain.**
    
    Serially because the first image was requested and recieved a response before the second image was requested. If there were in parallel both GET messages would have followed immediately after eachother.
### Section 5: HTTP Authentication 
18. **What is the server’s response (status code and phrase) in response to the initial HTTP GET message from your browser?**

    401 Authorization Required
19. **When your browser’s sends the HTTP GET message for the second time, what new field is included in the HTTP GET message?**
    A basic authenticaiton field. For "basic" authentication, the username and password are combined with a colon and then encoded in base64. Unfortunately, this is very easy to reverse making basic very insecure.
