### **R5.** What information is used by a process running on one host to identify a process running on another host?
To send a message to another process the sending process needs the recievers IP address and its port number. This is the address and the receiving process running in the host, respectively.

### **R8.** List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.
1. Reliable Data Transfer - TCP
2. Throughput - Neither
3. Timing - Neither
4. Security - TCP with TLS

Today's internet can often provide satisfactory service to time-sensitive applications but it cannot provide any throughput or timing gurantees. 

### **R11.** Why do HTTP, SMTP, and IMAP run on top of TCP rather than UDP
HTTP, SMTP, and IMAP all run on TCP because TCP provides reliable data transfer service while UDP does not. This means that they can count on TCP to deliver all of data to be sent without error and in the proper order which is crucial when requesting web server data, email, and internet messages. 

### **R12.** Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.
When a new client sends a request to the e-commerce site, the server creates a unique id for the user and stores it in their database. The site would use cookies to keep track of the purchase record associated with each id. Additonally, it will include a *Set-cookie: ####* header line in the HTTP response msg which the browser appends to its cookie file. Now, whenever the user vists the same site the cookie id number will be included in the request msg allowing the server to know which user it is and their purchase record. 

### **R13.** Describe how Web caching can reduce the dealy in receiving a requested object. Will web caching reduce the delay for all object requested by a user or for only some of the objects? Why?
Web caching will reduce delay because when a user requests an object that is stored in the cache it can be sent right back without having to request the object from a server. It only reduces the delay for the objects that are present in the cache. This usually is frequently visited objects. However, if the object is not in the cache it will still have to request it from the server which will not reduce delay.

### **R14.** Telnet into a Web server and send a multiline request message. Include in the request message the *If-Modified-since:* header line to force a response message with *304 Not Modified* status code.
GET / HTTP/1.1  
Host: go.com  
If-Modified-Since: Sun, 4 Sep 2023 13:45:20 EST

This the the GET request I put into the terminal after the command *telnet go.com 80*. I chose go.com because it was one of the few websites I found that still use HTTP and not HTTPS. My problem, however, was that the server still responded with the entire message. This might have something to do with my cache not working as it should or I am entering something with improper format.  

### **R26.** In **Section 2.7**, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support n simultaneous connections, each from a different client host, how many sockets would the TCP server need?
The TCP server needs two sockets because one socket acts as the welcoming socket to establish a connection between the client and tcp server. The welcoming socket then creates a new socket that is dedicated to the client where both the client and the server processes can send messages to each other over the connection at the same time. You would need n+1 sockets.

