#Owen Pitchford

class entry:

    def __init__(self, network, interface):
        self.network = network
        self.interface = interface

def ip_to_binary(ipv4_addr):
    return ''.join([bin(int(x)+256)[3:] for x in ipv4_addr.split('.')])


def match(routing_table, user_ip_binary):
    match = None
    longest_length = 7 #if less than 8 match then go to default
    
    for entry in routing_table[:-1]:
        curr_network = entry.network
        curr_interface = entry.interface

        #getting subnet and subnet mask
        split = curr_network.split("/")
        curr_ip_binary = ip_to_binary(split[0])
        mask = int(split[1]) #24
        
        #comparing user ip to routing table bit by bit
        length = 0
        for bit1, bit2 in zip(user_ip_binary, curr_ip_binary):
            if(bit1 == bit2):
                length += 1
            else:
                break 
        
        #make new match if more bits match up and the new amount of bits is
        #greater than or equal to the new subnet mask
        if(length > longest_length and length >= mask):
            longest_length = length
            match = curr_interface

    if(match == None):
        match = routing_table[-1].interface
    return match

routing_table = []
routing_table.append(entry("1.0.0.0/8", "Fa 0/0"))
routing_table.append(entry("1.100.56.0/24", "Fa 2/0"))
routing_table.append(entry("1.0.100.0/24", "Fa 0/1"))
routing_table.append(entry("1.99.0.0/20", "Fa 2/1"))
routing_table.append(entry("8.0.0.0/27", "Fa 0/2"))
routing_table.append(entry("8.8.8.0/24","Fa 2/2"))
routing_table.append(entry("126.2.3.0/20", "Fa 0/3"))
routing_table.append(entry("8.8.8.16/30", "Fa 2/3"))
routing_table.append(entry("237.1.1.0/30", "Fa 0/4"))
routing_table.append(entry("237.2.0.0/16","Fa 2/4"))
routing_table.append(entry("99.31.4.0/22","Fa 0/5"))
routing_table.append(entry("99.0.0.0/8","Fa 2/5"))
routing_table.append(entry("101.5.0.0/16", "Fa 0/6"))
routing_table.append(entry("223.0.0.0/8", "Fa 2/6"))
routing_table.append(entry("101.5.7.32/30", "Fa 0/7"))
routing_table.append(entry("99.0.0.0/20","Fa 2/7"))
routing_table.append(entry("101.0.0.0/8", "Gi 1/0"))
routing_table.append(entry("101.42.3.0/24", "Gi 3/0"))
routing_table.append(entry("223.4.0.0/16", "Gi 1/1"))
routing_table.append(entry("default", "Gi 3/1"))

print(match(routing_table, input()))