## How to Run
1. Type python3 RouterSim.py into the terminal and hit enter
2. Type an IPv4 address in binary into the terminal
3. The program will output which interface the ip address will go to on the next hop
