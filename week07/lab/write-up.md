# Introduction
For this lab, I made a simple router simulation in python. A user provides the program with an IPv4 address in binary and the program will output the next hop interface for that address. Throughout this lab, I developed my python coding skills as I am not very used to coding in python.

# Collaboration
[IPv4 to Binary Converter-Stack Overflow](https://stackoverflow.com/questions/2733788/convert-ip-address-string-to-binary-in-python) To assist with converting the IP addresses in the routing table I made use of the first answer in stack overflow. I just modified the '.' to '' for the join because I did not want the converted binary to be separated by periods. 

# Process
I followed the [Lab Notes](https://ic322.com/#/assignments/week07/lab-router-simulator/) on the course website.

# Questions
N/A

# Extra Notes
My program has a bug when determining if the correct interface should be Fa 0/5 or Fa 0/7. This might be happening because I might not fully understand longest prefix matching. I only change over to the smaller subnet if the longest matching length is greater than or equal to the subnet mask. Maybe since the mask is 20 it only has to be the 16 bits?

## Program Upgrades
This could be made better by error checking the user input as well as allowing the user to input either binary or dotted quad notation for the ip address. 

