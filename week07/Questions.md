## R11. Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).
Packet queuing can occur at input ports if the rate that packets are arriving at the swtihc fabric exceeds how fast the fabric can transfer the arriving packets through the fabric. Eventually, the queues will get large enough to a point where there is no more memory available to store the packets. This is a buffer overflow and the packets will be dropped. To eliminate packet loss at input ports, the switching fabric rate must be N times faster than the rate of packets coming into the input port where N is the number of input ports.  
## R12. Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?
Packet queing can occur at output ports even if the switching fabric in N times faster than the port line speeds because the output port can only transmit a single packet per packet tranmission time. So N packets can keep arriving and then queing while only one packet in transmitted. When there is not enough memory to buffer the packets, packet loss will occur. No, this loss cannot be prevented by increasing the switch fabric speed because it doesn't change the packet tranmission times which is what will clear the output buffer. 
## R13. What is HOL blocking? Does it occur in input ports or output ports?
HOL blocking occurs in input ports and it is when a packet that is first in line at an input port cannot go to its output port whether because the output buffer is full or the switching fabric is transferring another packet to the same port. Now, all the packets behind this packet are stuck and can't go to their output ports if there is no contention for them. 
## R16. What is an essentnial difference between Round Robin (RR) and Weighted Fair Queing (WFQ) packet scheduling? Is there a case (*Hint*: Consider the WFQ weights) where RR and WFQ will behave exactly the same?
WFQ differs from RR in that each class may recieve a differential amount of service in any interval of time. Essentially, each class will receive a certain weight that factors into the amount of bandwidth a class will receive. This is calculated by (weight of class/sum of all of weights of classese with queued packets). In RR, a scheduler aternates service equally among the classes. WFQ and RR will behave exactly the same if all the of the weights between classes in WFQ are the same. 
## R18. What field in the IP header can be used to ensure that a packet is forwarded through no more than N routers?
The Time-to-live field. Each time the datagram is processed by a router, N decreases by 1. Once TTL reaches 0, a router must drop the datagram.
## R21. Do routers have IP addresses? If so, how many?
Yes, but the IP address is associated with an interface that the router contains rather than the interface itself. An interface is the boundary between the router and any one of its links. The router will have ip addresses for each of its interfaces. 

## P4. Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle? ![Alt text](image-1.png)
The minimal number of time slots needed is three.
- Slot 1: Send X in the top input queue, and send Y in the middle input queue.
- Slot 2: Send Y in the middle and X in the bottom
- Slot 3: Send Z in the bottom input queue

The largest number of slots for the worst case is also three. Assuming no input queue can be idle while it has a packet in it, in this case, we always have to send two packets in the first and second time slot and then the last packet in the 3rd time slot.

## P5. Suppose that the WEQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .)
1123 1123 1123....  
Since class 1 has twice the amount of weight, twice as many packets will be sent for that class
### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?
112 112 112 ...
## P8. Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows: ![Alt text](image-2.png)
### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.
11100000 00 -> 0  
11100000 01000000 -> 1  
11100000 01 -> 2  
11100001 0 -> 2  
otherwise -> 3  
### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses: ![Alt text](image-4.png)
- 11001000 10010001 01010001 01010101: doesn't match bc starts with 11 so link interface 3  
- 11100001 01000000 11000011 00111100: longest match is 1110000 so interface 2  
- 11100001 10000000 00010001 01110111: matches 11100000 1 so interface 3

## P9. Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table. ![Alt text](image.png)
| Prefix Match | Interface | Destination Address Range|
|--------------|-----------|--------------------------|
| 00           | 0         | 00000000 - 00111111      |
| 010          | 1         | 01000000 - 01011111      |
| 011          | 2         | 01100000 - 01111111      |
| 10           | 2         | 10000000 - 10111111      |
| 11           | 3         | 11000000 - 11111111      |

## P11. Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.
- Subnet A: 223.1.17.0/26 (1-60 < 64 so 6 bits)
- Subnet B: 223.1.17.128/25 (1-90 < 128 so 7 bits)
- Subnet C: 223.1.17.64/28 (1-12 < 16 so 4 bits)  

I am honestly not too sure on this one and still confused from class today.
