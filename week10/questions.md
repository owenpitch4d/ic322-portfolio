# Review Questions
## **R11.** How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
- BGP uses AS-PATH attribute when choosing which route to take when there are multiple paths to the same prefix. AS-PATH is also used to detect and prevent looping advertisements; when a ruter sees its own AS in the path list, it rejects the advertisement.
- BGP uses the NEXT-HOP attribute when making its forwarding table as the NEXT-HOP attribute is the IP address of the router of the interface that begins the AS-PATH. The IP address of the router does not belong to the AS but the subnet that contains the IP address is directly attached to the AS. 

## **R13.** True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.
False. Suppose a router in one AS learns about a path to another AS. It can add its identity to the path so itself can route to the destination but it does not have to advertise this to its neighbors or then it would have to carry the traffic of other routers going through it to get to the final destination. 
## **R19.** ​Names four different types of ICMP messages
|ICMP Type|Code|Description|
|---------|----|-----------|
|0        |0   |echo reply (to ping)|
|3        |0   |destination network unreachable|
|3        |3   |destination port unreachable|
|11       |0   |TTL expired|
|12       |0   |IP header bad|

## **R20.** What two types of ICMP messages are received at the sending host executing the Traceroute program?
Type 11 code 0 and Type 3 code 3.

# Problems
## **P14.**  Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.![Alt text](image.png)

### **a.** Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?
eBGP
### **b.** Router 3a learns about x from which routing protocol?
iBGP
### **c.** Router 1c learns about x from which routing protocol?
eBGP
### **d.** Router 1d learns about x from which routing protocol? 
iBGP
## **P15.** Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.
### **a.** Will I be equal to I<sub>1</sub> or I<sub>2</sub> for this entry? Explain why in one sentence.
I<sub>1</sub> because it goes in the direction of the path with the least cost to the gateway router 1c and therefore the least cost to x. 
### **b.** Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I<sub>1</sub> or I<sub>2</sub>? Explain why in one sentence.
I<sub>2</sub> because even though both routes have the same AS-PATH length, I<sub>2</sub> starts the shortest path to Gateway router 1b which has the closest NEXT-HOP router 2a. 
### **c.** Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I<sub>1</sub> or I<sub>2</sub>? Explain why in one sentence.
I<sub>1</sub> because that route has the shortes AS-PATH.

## **P19.** In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
![Alt text](image-1.png)
- A to B: AS-PATHS A-W and A-V
- A to C: AS-PATHS A-V
- To C: A-V, B-A-W, B-A-V

## **P20.** Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​
No, because Z will have to send route advertisements to Y since it wants to transmit Y's traffic but becuase Y and X have a peering agreement, Y can re-advertise Z's routes to X allowing X to transmit through Y without Z being able to stop it. 
